<?php

namespace October\Demo\Models;

use Model;

/**
 * Movie Model
 */
class Cinema extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'cinamon_cinemas';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Hidden fields
     */
    protected $hidden = ['id', 'version'];

    /**
     * @var boolean Disable timestamps
     */
    public $timestamps = false;

    public function sessions()
    {
        return $this->hasMany(Session::class, 'cinema_id');
    }
}
