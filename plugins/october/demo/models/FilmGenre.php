<?php

namespace October\Demo\Models;

use Model;

/**
 * Genre Model
 */
class FilmGenre extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'cinamon_film_genres';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Hidden fields
     */
    protected $hidden = ['id', 'version'];

    /**
     * Disable timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @var array Jsonable fields
     */
    protected $jsonable = [
        'name_translations',
        'description_translations'
    ];

    /**
     * @var array Relations
     */
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
