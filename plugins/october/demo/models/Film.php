<?php

namespace October\Demo\Models;

use Model;

/**
 * Genre Model
 */
class Film extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'cinamon_films';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Hidden fields
     */
    protected $hidden = ['id', 'version'];

    /**
     * Disable timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @var array Jsonable fields
     */
    protected $jsonable = [
        'name_translations',
        'synopsis_translations'
    ];

    /**
     * @var array Relations
     */
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
