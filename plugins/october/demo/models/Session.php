<?php

namespace October\Demo\Models;

use Model;
use Carbon\Carbon;

/**
 * Genre Model
 */
class Session extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'cinamon_sessions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Hidden fields
     */
    protected $hidden = ['id', 'version', 'film_id', 'cinema_id'];

    /**
     * Disable timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'showtime'
    ];

    public function film()
    {
        return $this->belongsTo(Film::class, 'film_id');
    }
}
